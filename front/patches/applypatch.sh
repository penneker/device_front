#!/bin/sh
#
# applypatch.sh
# apply patches
#


dir=`cd $(dirname $0) && pwd`
top=$dir/../../../..

if [ "$RR" = "1" ]; then
    echo "*** This ResurrectionRemix ***"
    for patch in `ls $dir/RR/*.patch` ; do
        echo ""
        echo "==> patch file: ${patch##*/}"
        patch -p1 -N -i $patch -r - -d $top
    done
else
    echo "*** This ResurrectionRemix ***"
    for patch in `ls $dir/RR/*.patch` ; do
        echo ""
        echo "==> patch file: ${patch##*/}"
        patch -p1 -N -i $patch -r - -d $top
    done
fi

find . -name "*.orig" -delete
